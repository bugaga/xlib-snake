#ifndef DEFAULT_SETTINGS
#define DEFAULT_SETTINGS

// game
#define GRID_CELL_SIZE 10
#define GRID_WIDTH 50
#define GRID_HEIGHT 30

// snake
#define START_HEAD_POSITION_GX 10
#define START_HEAD_POSITION_GY 10
#define START_HEAD_DIRECTION "up"
// size of snake body (include head)
#define START_SNAKE_SIZE 4

// window
#define WINDOW_X 0
#define WINDOW_Y 0
#define WINDOW_WIDTH GRID_CELL_SIZE * GRID_WIDTH
#define WINDOW_HEIGHT GRID_CELL_SIZE * GRID_HEIGHT
#define WINDOW_BORDER_WIDTH 1
#define WINDOW_TITLE "Example"
#define WINDOW_ICON_TITLE "Example"
#define WINDOW_PRG_CLASS "Example"

#define gX2pxX(gx) gx * GRID_CELL_SIZE
#define gY2pxY(gy) gy * GRID_CELL_SIZE

#endif