#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

#include "default.h"

typedef enum { UP,
               RIGHT,
               DOWN,
               LEFT } Direction;

typedef struct
{
    int gx;
    int gy;
    Direction direction;
    enum
    {
        BODY,
        HEAD
    } type;
} SnakeBodyItem;

typedef struct
{
    int size;
    SnakeBodyItem *body;
} Snake;

Window initialize_window(Display *display)
{
    int screen_number = DefaultScreen(display);
    Window window = XCreateSimpleWindow(
        display,
        RootWindow(display, screen_number),
        WINDOW_X, WINDOW_Y, WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_BORDER_WIDTH,
        BlackPixel(display, screen_number),
        WhitePixel(display, screen_number));

    XSizeHints hints;
    hints.flags = PPosition | PSize | PMinSize | PMaxSize;
    hints.min_height = WINDOW_HEIGHT;
    hints.min_width = WINDOW_WIDTH;
    hints.max_height = WINDOW_HEIGHT;
    hints.max_width = WINDOW_WIDTH;
    XSetWMNormalHints(display, window, &hints);

    return window;
}

void initialize_snake(Snake *snake)
{
    snake->size = START_SNAKE_SIZE;

    if (snake->size < 2)
    {
        printf("Start size of snake cannot be less than 2!");
        exit(1);
    }

    SnakeBodyItem *body = (SnakeBodyItem *)malloc(sizeof(SnakeBodyItem) * snake->size);

    if (body == NULL)
    {
        printf("Cannot get %ld memmory", sizeof(SnakeBodyItem) * snake->size);
        exit(1);
    }

    char *start_direction = (char *)START_HEAD_DIRECTION;
    Direction direction;

    if (strcmp("up", start_direction) == 0)
    {
        direction = UP;
    }
    else if (strcmp("right", start_direction) == 0)
    {
        direction = RIGHT;
    }
    else if (strcmp("down", start_direction) == 0)
    {
        direction = DOWN;
    }
    else if (strcmp("left", start_direction) == 0)
    {
        direction = LEFT;
    }
    else
    {
        printf("Invalid direction %s\n", start_direction);
        exit(1);
    }

    body[0].type = HEAD;
    body[0].direction = direction;
    body[0].gx = START_HEAD_POSITION_GX;
    body[0].gy = START_HEAD_POSITION_GY;

    for (int i = 1; i < snake->size; i++)
    {
        int gx, gy;

        body[i].type = BODY;
        body[i].direction = direction;

        switch (direction)
        {
        case UP:
            gx = body[i - 1].gx;
            gy = body[i - 1].gy + 1;
            break;

        case DOWN:
            gx = body[i - 1].gx;
            gy = body[i - 1].gy - 1;
            break;

        case RIGHT:
            gx = body[i - 1].gx;
            gy = body[i - 1].gy - 1;
            break;

        case LEFT:
            gx = body[i - 1].gx + 1;
            gy = body[i - 1].gy;
            break;
        }

        if (gx < 0 || gy < 0)
        {
            printf("Invalid coordinates: (%d; %d)\n", gx, gy);
            exit(1);
        }

        body[i].gx = gx;
        body[i].gy = gy;
    }

    snake->body = body;
}

void draw_grid(Display *display, Window window)
{
    GC gc = XCreateGC(display, window, 0, NULL);
    XSetForeground(display, gc, BlackPixel(display, DefaultScreen(display)));

    for (int i = 1; i < GRID_WIDTH; i++)
    {
        XDrawLine(display, window, gc, gX2pxX(i), 0, gX2pxX(i), WINDOW_HEIGHT);
    }

    for (int i = 1; i < GRID_HEIGHT; i++)
    {
        XDrawLine(display, window, gc, 0, gY2pxY(i), WINDOW_WIDTH, gY2pxY(i));
    }

    XFreeGC(display, gc);
}

void draw_snake_body_item(Display *display, Window window, int gx, int gy, XColor *color)
{
    XGCValues gc_values;
    gc_values.foreground = color->pixel;
    GC gc = XCreateGC(display, window, GCForeground, &gc_values);
    XFillRectangle(display, window, gc, gX2pxX(gx) + 1, gY2pxY(gy) + 1, GRID_CELL_SIZE - 1, GRID_CELL_SIZE - 1);
    XFreeGC(display, gc);
}

void draw_snake(Display *display, Window window, const Snake *snake)
{
    Colormap screen_colormap = XDefaultColormap(display, DefaultScreen(display));
    XColor red, blue, exact_color;
    Status rc = XAllocNamedColor(display, screen_colormap, "red", &red, &exact_color) && XAllocNamedColor(display, screen_colormap, "blue", &blue, &exact_color);

    if (rc == 0)
    {
        printf("XAllocColor Error\n");
        exit(-1);
    }

    draw_snake_body_item(display, window, snake->body[0].gx, snake->body[0].gy, &red);

    for (int i = 1; i < snake->size; i++)
    {
        draw_snake_body_item(display, window, snake->body[i].gx, snake->body[i].gy, &blue);
    }
}

int main(int argc, char *argv[])
{
    Display *display;
    XEvent event;
    Window window;
    Snake snake;

    initialize_snake(&snake);

    if ((display = XOpenDisplay(NULL)) == NULL)
    {
        puts("Can not connect to the X server!\n");
        return 1;
    }

    window = initialize_window(display);

    XSelectInput(display, window, ExposureMask | KeyPressMask);
    XMapWindow(display, window);

    short work = 1;
    while (work)
    {
        XNextEvent(display, &event);

        switch (event.type)
        {
        case Expose:
            if (event.xexpose.count != 0)
                break;

            XClearWindow(display, window);

            draw_grid(display, window);
            draw_snake(display, window, &snake);

            XFlush(display);
            break;

        case KeyPress:
            work = 0;
        }
    }

    XCloseDisplay(display);

    return 0;
}